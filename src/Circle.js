import Measure from 'react-measure'
import React from 'react';
import circle from "./assets/circle.png";

export default class Circle extends React.Component {
    state = {
        dimensions: {
            width: -1,
            height: -1,
        },
    }

    render() {
        const { width, height } = this.state.dimensions
        return (
            <div style={{display:'flex',flexDirection:'column',justifyContent:'center',alignItems:'center'}}>
                {this.props.voltage}
                <Measure
                    bounds
                    onResize={contentRect => {
                        this.setState({ dimensions: contentRect.bounds })
                    }}
                >
                    {({ measureRef }) => (
                        <img ref={measureRef} src={circle} alt="circle" className='circle'/>
                    )}
                </Measure>
            </div>
        )
    }
}
