import nerdamer from 'nerdamer/nerdamer.core.js'
import 'nerdamer/Algebra.js'
import 'nerdamer/Calculus.js'
import 'nerdamer/Solve.js'
import React from 'react';
import close from './assets/close.png';
import ARS from './assets/ARS.PNG';
import AS from './assets/AS.PNG';
import R from './assets/R.PNG';
import VRS from './assets/VRS.PNG';
import VS from './assets/VS.PNG';
import wire from './assets/wire.png';
import Circle from './Circle'

import './App.css';
import Modal from 'react-modal';
import {withContentRect} from 'react-measure'

const customStyles = {
    content: {
        display: 'flex',
        justifyContent: 'space-between',
        alignItems: 'flex-end',
        flexDirection: 'column'
    }
};


Modal.setAppElement('#root');
export default class App extends React.Component {


    constructor() {
        super();

        this.state = {
            modalIsOpen: false,
            chosenCircuitItem: 'ARS',
            graph: [
                [
                    false,
                    {type: 'R', value: 1},
                    false,
                    false,
                    false,
                    {type: 'R', value: 1},
                    false,
                    false,
                    false,
                ], [
                    {type: 'R', value: 1},
                    false,
                    {type: 'VRS', s1: 0, s2: 1, z: 2, b: 1},
                    false,
                    {type: 'W', seen: false, kcl: true},
                    false,
                    false,
                    false,
                    false,
                ], [
                    false,
                    {type: 'VRS', s1: 0, s2: 1, z: -2, b: -1},
                    false,
                    {type: 'W', seen: false, kcl: true},
                    false,
                    false,
                    false,
                    false,
                    false,
                ], [
                    false,
                    false,
                    {type: 'W', seen: false, kcl: true},
                    false,
                    {type: 'AS', seen: false, value: 2},
                    false,
                    false,
                    false,
                    {type: 'W', seen: false, kcl: true},
                ], [
                    false,
                    {type: 'W', seen: false, kcl: true},
                    false,
                    {type: 'AS', seen: false, value: -2},
                    false,
                    {type: 'R', value: 2},
                    false,
                    false,
                    false,
                ], [
                    {type: 'R', value: 1},
                    false,
                    false,
                    false,
                    {type: 'R', value: 2},
                    false,
                    {type: 'W', seen: false, kcl: true},
                    false,
                    false,
                ], [
                    false,
                    false,
                    false,
                    false,
                    false,
                    {type: 'W', seen: false, kcl: true},
                    false,
                    {type: 'ARS', s1: 4, s2: 5, z: 1, b: 4},
                    false,
                ], [
                    false,
                    false,
                    false,
                    false,
                    false,
                    false,
                    {type: 'ARS', s1: 4, s2: 5, z: -1, b: -4},
                    false,
                    {type: 'W', seen: false, kcl: true},
                ], [
                    false,
                    false,
                    false,
                    {type: 'W', seen: false, kcl: true},
                    false,
                    false,
                    false,
                    {type: 'W', seen: false, kcl: true},
                    false,
                ]
            ],
            calculatedVoltages: ['', '', '', '', '', '', '', '', ''],
        };

        this.openModal = this.openModal.bind(this);
        this.afterOpenModal = this.afterOpenModal.bind(this);
        this.closeModal = this.closeModal.bind(this);
        this.wireEquation = this.wireEquation.bind(this);
        this.voltageSourceEquation = this.voltageSourceEquation.bind(this);
        this.voltageRelativeSourceEquation = this.voltageRelativeSourceEquation.bind(this);
        this.KCL = this.KCL.bind(this);
        this.addItem = this.addItem.bind(this);
        this.calculate = this.calculate.bind(this)
    }

    getCircuitItems() {
        let i = 0;
        let circuitItem = [];
        for (; i < this.state.graph.length; i += 1) {
            let j = i;
            for (; j < this.state.graph[i].length; j += 1) {
                if (this.state.graph[i][j] !== false) {
                    let attributes=this.attributeCalculator(i,j);
                    circuitItem.push(
                        <img key={i*50 + j*1000} src={this.state.graph[i][j] === 'ARS' ? ARS
                            : this.state.graph[i][j].type === 'AS' ? AS
                                : this.state.graph[i][j].type === 'R' ? R
                                    : this.state.graph[i][j].type === 'VRS' ? VRS
                                        : this.state.graph[i][j].type === 'VS' ? VS
                                            : this.state.graph[i][j].type === 'W' ? wire : ARS
                        } alt="chosen circuit item" style={{position:'absolute',width:attributes.width+'px',height:attributes.height+'px',top:attributes.top+'px',left:attributes.left+'px',transform: `rotate(${attributes.angel}deg)`}}/>
                    )
                }
            }
        }
        return circuitItem
    }

    openModal() {
        this.setState({modalIsOpen: true});
    }

    afterOpenModal() {
        // references are now sync'd and can be accessed.
    }

    closeModal() {
        this.setState({modalIsOpen: false});
    }

    addItem() {
        let temp = this.state.graph;
        if (this.state.chosenCircuitItem === 'W') {
            temp[parseInt(this.input5.value)][parseInt(this.input6.value)] = {
                type: 'W',
                seen: false,
                kcl: true
            };
            temp[parseInt(this.input6.value)][parseInt(this.input5.value)] = {
                type: 'W',
                seen: false,
                kcl: true
            }
        }
        if (this.state.chosenCircuitItem === 'R') {
            temp[parseInt(this.input5.value)][parseInt(this.input6.value)] = {
                type: 'R',
                value: parseFloat(this.input6.value)
            };
            temp[parseInt(this.input6.value)][parseInt(this.input5.value)] = {
                type: 'R',
                value: parseFloat(this.input6.value)
            }

        }
        if (this.state.chosenCircuitItem === 'AS') {
            temp[parseInt(this.input5.value)][parseInt(this.input6.value)] = {
                type: 'AS',
                seen: false,
                value: parseFloat(this.input6.value)
            };
            temp[parseInt(this.input6.value)][parseInt(this.input5.value)] = {
                type: 'AS',
                seen: false,
                value: -parseFloat(this.input6.value)
            }
        }
        if (this.state.chosenCircuitItem === 'VS') {
            temp[parseInt(this.input5.value)][parseInt(this.input6.value)] = {
                type: 'VS',
                seen: false,
                value: parseFloat(this.input6.value)
            };
            temp[parseInt(this.input6.value)][parseInt(this.input5.value)] = {
                type: 'VS',
                seen: false,
                value: -parseFloat(this.input6.value)
            }
        }
        if (this.state.chosenCircuitItem === 'VRS') {
            temp[parseInt(this.input5.value)][parseInt(this.input6.value)] = {
                type: 'VRS',
                s1: parseInt(this.input1.value),
                s2: parseInt(this.input2.value),
                z: parseFloat(this.input3.value),
                b: parseFloat(this.input4.value)
            };
            temp[parseInt(this.input6.value)][parseInt(this.input5.value)] = {
                type: 'VRS',
                s1: parseInt(this.input1.value),
                s2: parseInt(this.input2.value),
                z: -parseFloat(this.input3.value),
                b: -parseFloat(this.input4.value)
            }
        }
        if (this.state.chosenCircuitItem === 'ARS') {
            temp[parseInt(this.input5.value)][parseInt(this.input6.value)] = {
                type: 'ARS',
                s1: parseInt(this.input1.value),
                s2: parseInt(this.input2.value),
                z: parseFloat(this.input3.value),
                b: parseFloat(this.input4.value)
            };
            temp[parseInt(this.input6.value)][parseInt(this.input5.value)] = {
                type: 'ARS',
                s1: parseInt(this.input1.value),
                s2: parseInt(this.input2.value),
                z: -parseFloat(this.input3.value),
                b: -parseFloat(this.input4.value)
            }
        }
        this.setState({graph: temp}, () => {
            console.log(this.state.graph)
        })
    }

    wireEquation(i, j) {
        let newEquation = [];
        let k = 0;
        for (; k < this.state.graph.length + 1; k++)
            newEquation.push(0);
        newEquation[i] += 1;
        newEquation[j] += -1;
        let temp = this.state.graph;
        temp[i][j].seen = true;
        temp[j][i].seen = true;
        this.setState({graph: temp});
        return newEquation;
    }

    voltageSourceEquation(i, j, value) {
        let newEquation = [];
        let k = 0;
        for (; k < this.state.graph.length + 1; k++)
            newEquation.push(0);
        newEquation[i] += -1;
        newEquation[j] += +1;
        newEquation[this.state.graph.length] += value;
        let temp = this.state.graph;
        temp[i][j].seen = true;
        temp[j][i].seen = true;
        this.setState({graph: temp});
        return newEquation;
    }

    voltageRelativeSourceEquation(i, j, s1, s2, z, b) {
        let newEquation = [];
        let k = 0;
        for (; k < this.state.graph.length + 1; k++)
            newEquation.push(0);
        newEquation[i] += -1;
        newEquation[j] += +1;
        newEquation[s2] += -z;
        newEquation[s1] += +z;
        newEquation[this.state.graph.length] += -b;
        let temp = this.state.graph;
        temp[i][j].seen = true;
        temp[j][i].seen = true;
        this.setState({graph: temp});
        return newEquation;
    }

    canCheck(i) {
        let result = false;
        let j = 0;
        for (; j < this.state.graph.length; j++)
            if (this.state.graph[i][j]) {
                result = true;
                if (this.state.graph[i][j].type === 'VS' || this.state.graph[i][j].type === 'VRS')
                    return false;
                else if (this.state.graph[i][j].seen && !this.state.graph[i][j].kcl)
                    result = false;
            }
        return result;
    }

    exist(array, element) {
        let i = 0;
        for (; i < array.length; i++)
            if (array[i] === element) return true;
        return false;
    }

    equalEquation(a, b) {
        let i = 0;
        for (; i < this.state.graph.length + 1; i++)
            if (a[i] !== b[i]) return false;
        return true;
    }

    existEquation(array, element) {
        let i = 0;
        for (; i < array.length; i++)
            if (this.equalEquation(array[i], element)) return true;
        return false;
    }

    connectToS(i, last) {
        let j = 0;
        for (; j < this.state.graph.length; j++) {
            if (!this.state.graph[i][j]) continue;
            if (this.state.graph[i][j].type === 'VS' || this.state.graph[i][j].type === 'VRS') return true;
            if (this.exist(last, j)) continue;
            let temp = last;
            temp.push(i);
            if (this.state.graph[i][j].type === 'W' && this.connectToS(j, temp)) return true;
        }
        return false;
    }

    merge(a, b) {
        let c = [];
        let i = 0;
        for (; i < this.state.graph.length + 1; i++)
            c[i] = a[i] + b[i];
        return c;
    }

    KCL(i) {
        let newEquation = [];
        let k = 0;
        for (; k < this.state.graph.length + 1; k++)
            newEquation.push(0);
        let j = 0;
        for (; j < this.state.graph.length; j++) {
            if (!this.state.graph[i][j]) continue;
            if (this.state.graph[i][j].type === 'AS') {
                newEquation[this.state.graph.length] += this.state.graph[i][j].value;
            } else if (this.state.graph[i][j].type === 'ARS') {
                newEquation[this.state.graph[i][j].s1] += this.state.graph[i][j].z;
                newEquation[this.state.graph[i][j].s2] += -this.state.graph[i][j].z;
                newEquation[this.state.graph.length] += this.state.graph[i][j].b;
            } else if (this.state.graph[i][j].type === 'R') {
                newEquation[j] += 1 / this.state.graph[i][j].value;
                newEquation[i] += -1 / this.state.graph[i][j].value;
            } else if (this.state.graph[i][j].type === 'W' && this.state.graph[i][j].kcl && this.canCheck(j)) {
                let temp = this.state.graph;
                temp[i][j].kcl = false;
                temp[j][i].kcl = false;
                newEquation = this.merge(newEquation, this.KCL(j));
                temp[i][j].kcl = true;
                temp[j][i].kcl = true;
                this.setState({graph: temp})
            }
        }
        return newEquation;
    }

    equationMaker() {
        let equations = [];
        let i = 0;
        for (; i < this.state.graph.length; i++) {
            let j = 0;
            for (; j < this.state.graph.length; j++) {
                if (this.state.graph[i][j] && !this.state.graph[i][j].seen) {
                    if (this.state.graph[i][j].type === 'W')
                        equations.push(this.wireEquation(i, j));
                    else if (this.state.graph[i][j].type === 'VS')
                        equations.push(this.voltageSourceEquation(i, j, this.state.graph[i][j].value));
                    else if (this.state.graph[i][j].type === 'VRS')
                        equations.push(this.voltageRelativeSourceEquation(i, j, this.state.graph[i][j].s1, this.state.graph[i][j].s2, this.state.graph[i][j].z, this.state.graph[i][j].b))
                }
            }
        }

        i = 0;
        for (; i < this.state.graph.length; i++) {
            if (!this.canCheck(i)) continue;
            if (this.connectToS(i, [])) continue;
            let newEquation = this.KCL(i);
            if (!this.existEquation(equations, newEquation)) equations.push(newEquation);
        }
        return equations
    }


    iCalculator(i, j, voltages, last) {
        let value = 0;
        if (this.state.graph[i][j].type === 'AS')
            return this.state.graph[i][j].value;

        else if (this.state.graph[i][j].type === 'ARS') {
            value = this.state.graph[i][j].z * (voltages[this.state.graph[i][j].s2][1] - voltages[this.state.graph[i][j].s1][1]) + this.state.graph[i][j].b;
            return value;
        }
        else if (this.state.graph[i][j].type === 'R') {
            value = (voltages[i][1] - voltages[j][1]) / this.state.graph[i][j].value;
            return value;
        }
        else {
            value = 0;
            let k = 0;
            last.push(i)
            for (; k < this.state.graph.length; k++) {
                if (!this.state.graph[j][k] || k===i) continue;
                if (this.exist(last, k)) return 'noAnswer';
                let newValue = this.iCalculator(j, k, voltages, last);
                if (newValue === 'noAnswer') return 'noAnswer';
                else value += newValue;
            }
            return value;
        }
    }

    iListMaker(voltages) {
        let iList = []
        let i=0;
        for(; i < this.state.graph.length; i++) {
            let j = i+1;
            for(; j < this.state.graph.length; j++) {
                if (!this.state.graph[i][j]) continue;
                let value = this.iCalculator(i, j, voltages, []);
                if (value === 'noAnswer')
                    value = -this.iCalculator(j, i, voltages, []);
                if (value !== 'noAnswer')
                    iList.push([i, j, value]);
            }
        }
        return iList;
    }



    calculate() {
        let equationsInfo = this.equationMaker();
        let equations = equationsInfo.map((equationInfo) => {
            let equationString = '';
            let i = 0;
            const alphabet = ['a', 'b', 'c', 'd', 'f', 'g', 'h', 'i', 'j'];
            for (; i < equationInfo.length - 1; i += 1) {
                if (equationInfo[i]) {
                    if (equationInfo[i] > 0)
                        equationString += (equationInfo[i] + '*' + alphabet[i] + '+');
                    else {
                        equationString = equationString.slice(0, -1);
                        equationString += (equationInfo[i] + '*' + alphabet[i] + '+')
                    }
                }
            }
            if (equationInfo[equationInfo.length - 1] >= 0) {
                equationString += equationInfo[equationInfo.length - 1]
            } else {
                equationString = equationString.slice(0, -1);
                equationString += equationInfo[equationInfo.length - 1]
            }
            equationString += '=0';
            return equationString;
        });

        equations.push('a=0');
        let sol = nerdamer.solveEquations(equations, ['a', 'b', 'c', 'd', 'f', 'g', 'h', 'i', 'j']);
        this.setState({calculatedVoltages: sol.map((ans) => (ans[1]))}, () => {
            console.log(this.state.calculatedVoltages)
        })
        let Is = this.iListMaker(sol);
        console.log(Is);
    }

    getCoordinate(i, j) {
        let coordinates = {top1: 0, top2: 0, left1: 0, left2: 0};
        switch (i) {
            case 0:
                coordinates.top1 = this.c1.state.dimensions.top;
                coordinates.left1 = this.c1.state.dimensions.left;
                break;
            case 1:
                coordinates.top1 = this.c2.state.dimensions.top;
                coordinates.left1 = this.c2.state.dimensions.left;
                break;
            case 2:
                coordinates.top1 = this.c3.state.dimensions.top;
                coordinates.left1 = this.c3.state.dimensions.left;
                break;
            case 3:
                coordinates.top1 = this.c4.state.dimensions.top;
                coordinates.left1 = this.c4.state.dimensions.left;
                break;
            case 4:
                coordinates.top1 = this.c5.state.dimensions.top;
                coordinates.left1 = this.c5.state.dimensions.left;
                break;
            case 5:
                coordinates.top1 = this.c6.state.dimensions.top;
                coordinates.left1 = this.c6.state.dimensions.left;
                break;
            case 6:
                coordinates.top1 = this.c7.state.dimensions.top;
                coordinates.left1 = this.c7.state.dimensions.left;
                break;
            case 7:
                coordinates.top1 = this.c8.state.dimensions.top;
                coordinates.left1 = this.c8.state.dimensions.left;
                break;
            case 8:
                coordinates.top1 = this.c9.state.dimensions.top;
                coordinates.left1 = this.c9.state.dimensions.left;
                break;
            default:
                break;
        }
        switch (j) {
            case 0:
                coordinates.top2 = this.c1.state.dimensions.top;
                coordinates.left2 = this.c1.state.dimensions.left;
                break;
            case 1:
                coordinates.top2 = this.c2.state.dimensions.top;
                coordinates.left2 = this.c2.state.dimensions.left;
                break;
            case 2:
                coordinates.top2 = this.c3.state.dimensions.top;
                coordinates.left2 = this.c3.state.dimensions.left;
                break;
            case 3:
                coordinates.top2 = this.c4.state.dimensions.top;
                coordinates.left2 = this.c4.state.dimensions.left;
                break;
            case 4:
                coordinates.top2 = this.c5.state.dimensions.top;
                coordinates.left2 = this.c5.state.dimensions.left;
                break;
            case 5:
                coordinates.top2 = this.c6.state.dimensions.top;
                coordinates.left2 = this.c6.state.dimensions.left;
                break;
            case 6:
                coordinates.top2 = this.c7.state.dimensions.top;
                coordinates.left2 = this.c7.state.dimensions.left;
                break;
            case 7:
                coordinates.top2 = this.c8.state.dimensions.top;
                coordinates.left2 = this.c8.state.dimensions.left;
                break;
            case 8:
                coordinates.top2 = this.c9.state.dimensions.top;
                coordinates.left2 = this.c9.state.dimensions.left;
                break;
            default:
                break;
        }
        return coordinates;
    }

    attributeCalculator(i,j) {
        let coordinates=this.getCoordinate(i,j);
        let angel = Math.atan((coordinates.left1 - coordinates.left2) / (coordinates.top1 - coordinates.top2));
        let width = Math.sqrt((coordinates.top1 - coordinates.top2) * (coordinates.top1 - coordinates.top2) + (coordinates.left1 - coordinates.left2) * (coordinates.left1 - coordinates.left2));
        let height = width ;
        let top = (coordinates.top1 + coordinates.top2) / 2;
        let left = (coordinates.left1 + coordinates.left2) / 2;
        return {angel, width, height, top, left}
    }


    render() {

        return (
            <div className="App">
                <div>
                    {this.c1 && this.getCircuitItems()}
                </div>
                <div className="card">
                    <div className='circles'>
                        <Circle voltage={this.state.calculatedVoltages[0]} ref={(ref) => {
                            this.c1 = ref
                        }}/>
                        <Circle voltage={this.state.calculatedVoltages[1]} ref={(ref) => {
                            this.c2 = ref
                        }}/>
                        <Circle voltage={this.state.calculatedVoltages[2]} ref={(ref) => {
                            this.c3 = ref
                        }}/>
                    </div>
                    <div className='circles'>
                        <Circle voltage={this.state.calculatedVoltages[3]} ref={(ref) => {
                            this.c4 = ref
                        }}/>
                        <Circle voltage={this.state.calculatedVoltages[4]} ref={(ref) => {
                            this.c5 = ref
                        }}/>
                        <Circle voltage={this.state.calculatedVoltages[5]} ref={(ref) => {
                            this.c6 = ref
                        }}/>
                    </div>
                    <div className='circles'>
                        <Circle voltage={this.state.calculatedVoltages[6]} ref={(ref) => {
                            this.c7 = ref
                        }}/>
                        <Circle voltage={this.state.calculatedVoltages[7]} ref={(ref) => {
                            this.c8 = ref
                        }}/>
                        <Circle voltage={this.state.calculatedVoltages[8]} ref={(ref) => {
                            this.c9 = ref
                        }}/>
                    </div>
                </div>

                <div className='buttons'>

                    <button className='placePortButton' onClick={this.openModal}>Place Port</button>
                    <button className='calculateButton' onClick={this.calculate}>Calculate</button>

                </div>

                <Modal
                    style={customStyles}
                    isOpen={this.state.modalIsOpen}
                    onAfterOpen={this.afterOpenModal}
                    onRequestClose={this.closeModal}
                    contentLabel="Example Modal"
                >
                    {/*this.el.state.contentRect.bounds*/}
                    <img src={close} alt='close' className='closeButton' onClick={this.closeModal}/>
                    <div className='modalContent'>

                        <div className='chosenItem'>
                            <img src={this.state.chosenCircuitItem === 'ARS' ? ARS
                                : this.state.chosenCircuitItem === 'AS' ? AS
                                    : this.state.chosenCircuitItem === 'R' ? R
                                        : this.state.chosenCircuitItem === 'VRS' ? VRS
                                            : this.state.chosenCircuitItem === 'VS' ? VS
                                                : this.state.chosenCircuitItem === 'W' ? wire : ARS
                            } alt="chosen circuit item" className='chosenItemImage'/>
                            <div className='circuitItemInputs'>
                                <input type="text" ref={(input5) => {
                                    this.input5 = input5
                                }}/>
                                از نقطه
                                <input type="text" ref={(input6) => {
                                    this.input6 = input6
                                }}/>
                                به نقطه
                            </div>
                            <div className='circuitItemInputs'>
                                {this.state.chosenCircuitItem !== 'W' && <input type="text" ref={(input1) => {
                                    this.input1 = input1
                                }}/>}
                                {(this.state.chosenCircuitItem !== 'W' && this.state.chosenCircuitItem !== 'ARS' && this.state.chosenCircuitItem !== 'VRS') &&
                                <div>مقدار</div>}
                                {(this.state.chosenCircuitItem === "VRS" || this.state.chosenCircuitItem === "ARS") &&
                                (<div className={'circuitItemInputs'}>
                                    منبع 1
                                    <input type="text" ref={(input2) => {
                                        this.input2 = input2
                                    }}/>
                                    منبع 2
                                    <input type="text" ref={(input3) => {
                                        this.input3 = input3
                                    }}/>
                                    ضریب
                                    <input type="text" ref={(input4) => {
                                        this.input4 = input4
                                    }}/>
                                    بایاس
                                </div>)
                                }
                            </div>
                            <button className='calculateButton' onClick={this.addItem}>Add Item</button>
                        </div>
                        <div className='circuitItems'>
                            <img onClick={() => {
                                this.setState({chosenCircuitItem: 'ARS'})
                            }} src={ARS} alt="circle" className='circuitItem'/>
                            <img onClick={() => {
                                this.setState({chosenCircuitItem: 'AS'})
                            }} src={AS} alt="circle" className='circuitItem'/>
                            <img onClick={() => {
                                this.setState({chosenCircuitItem: 'R'})
                            }} src={R} alt="circle" className='circuitItem'/>
                            <img onClick={() => {
                                this.setState({chosenCircuitItem: 'VRS'})
                            }} src={VRS} alt="circle" className='circuitItem'/>
                            <img onClick={() => {
                                this.setState({chosenCircuitItem: 'VS'})
                            }} src={VS} alt="circle" className='circuitItem'/>
                            <img onClick={() => {
                                this.setState({chosenCircuitItem: 'W'})
                            }} src={wire} alt="circle" className='circuitItem'/>
                        </div>
                    </div>
                </Modal>

            </div>

        );
    }
}


